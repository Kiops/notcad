.import "object_creation.js" as Logic

function save(list, seals, saveModel)
{
    saveModel.clear();
    var bars = list.get_all();
    bars.forEach(function(bar, number, bars){
        bar = bar.data;
        saveModel.push_back(bar.area, bar.length, bar.max_strain,
                            bar.youngs_modulus, bar.ll_value,
                            bar.lpl_value, bar.rpl_value);
    });
    var left_seal = false;
    var right_seal = false;
    if(seals.left !== undefined)
        left_seal = true;
    if(seals.right !== undefined)
        right_seal = true;
    saveModel.set_seals(left_seal, right_seal);
}

function load(list, seals, pluses, bar_redactor, saveModel, filename){
    var all_bars = list.get_all();
    for(var num = 0; num < all_bars.length; num++)
    {
        Logic.remove_bar(list, pluses, all_bars[num].data, seals);
    }
    
    if(seals.left !== undefined)
        Logic.remove_seal(seals, seals.left);
    if(seals.right !== undefined)
        Logic.remove_seal(seals, seals.right);
    
    var bar_params = {};
    
    saveModel.read_from_file(filename);
    var m_lengths = saveModel.get_lengths();
    var m_areas = saveModel.get_areas();
    var m_maxStrains = saveModel.get_maxStrains();
    var m_youngsModuluses = saveModel.get_youngsModuluses();
    var m_llValues = saveModel.get_llValues();
    var m_lplValues = saveModel.get_lplValues();
    var m_rplValues = saveModel.get_rplValues();
    
    var last_added = 0;
    for(var number = 0; number < m_lengths.length; number++)
    {   
        bar_params.left_neighbor = number > 0 ? last_added : undefined;
        bar_params.ll_value = m_llValues[number];
        bar_params.lpl_value = m_lplValues[number];
        bar_params.rpl_value = m_rplValues[number];
        bar_params.youngs_modulus = m_youngsModuluses[number];
        bar_params.max_strain = m_maxStrains[number];
        bar_params.length = m_lengths[number];
        bar_params.area = m_areas[number];
        
        last_added = Logic.create_bar(list, bar_redactor, bar_params, seals);
    }
    if(saveModel.right_seal_exist())
        Logic.create_seal(list, seals, false);
    if(saveModel.left_seal_exist())
        Logic.create_seal(list, seals, true);
}
