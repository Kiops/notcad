import QtQuick 2.7
import QtQuick.Controls 2.0


Rectangle{
    id: main_rect
    
    property alias textInput: txt
    property alias text: txt.text
    property alias acceptableInput: txt.acceptableInput
    
    property bool only_positive: false
    
    signal inputFinished()
    
    radius: 5
    color: "transparent"
    border.color: "#EEEEEE"
    width: childrenRect.width+5
    height: childrenRect.height
    property bool disabled: false
    
    TextInput{
        id: txt
        width: contentWidth > 50 ? contentWidth : 50
        mouseSelectionMode: TextEdit.SelectCharacters
        horizontalAlignment: TextEdit.AlignHCenter
        selectionColor: "#B0BEC5"
        selectByMouse: true
        readOnly: disabled
        wrapMode: TextEdit.NoWrap
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        validator: DoubleValidator {id: validator; bottom: main_rect.only_positive ? 0.000000001 : -Infinity;}
        onEditingFinished:
            main_rect.inputFinished();
    }
    
    onActiveFocusChanged: {if(activeFocus) txt.forceActiveFocus()}
}
