import KiopsQML 1.0
import QtQuick 2.7

Item {
    
    property alias text: label.text
    property var connected_bar: undefined
    TextInput{
        id: label
        color: "#EEEEEE"
        font.pixelSize: 25
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: arrow.top
        validator: DoubleValidator {bottom: 0.000000001;}
        onEditingFinished:{
            acceptableInput ? connected_bar.length = text : text = connected_bar.length
        }
    }
    
    ArrowView{
        id: arrow
        typeOfArrow: 1
        color: "#A5D6A7"
        
        height: 20
        width: parent.width
        anchors.centerIn: parent
    }
}
