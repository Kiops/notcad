#pragma once

class Bar
{
    double area; //площадь сечения
    double length; //длина
    double max_strain; //допускаемое напряжение
    double youngs_modulus; //модуль упругости (модуль юнга)
    
    double linear_load; //распределённая нагрузка
    double right_load; //нагрузка в правом конце
    double left_load; //нагрузка в левом конце
public:
    Bar()
    {
        this->area = 0;
        this->length = 1;
        this->max_strain = 0;
        this->youngs_modulus = 0;
        this->linear_load = 0;
        this->right_load = 0;
        this->left_load = 0; 
    }
    Bar(double length, double area, double max_strain, double youngs_modulus, 
        double linear_load, double right_load, double left_load)
    {
        this->youngs_modulus = youngs_modulus;
        this->linear_load = linear_load;
        this->max_strain = max_strain;
        this->right_load = right_load;
        this->left_load = left_load;
        this->length = length;
        this->area = area;
    }

    void set_length(double length) { this->length = length; }
    void set_square(double square) { this->area = square; }
    void set_max_strain(double max_strain) { this->max_strain = max_strain; }
    void set_youngs_modulus(double youngs_modulus) { this->youngs_modulus = youngs_modulus; }
    void set_linear_load(double linear_load) {this->linear_load = linear_load;}
    void set_right_load(double right_load) {this->right_load = right_load;}
    void set_left_load(double left_load) {this->left_load = left_load;}

    double get_length() { return length; }
    double get_area() { return area; }
    double get_max_strain() { return max_strain; }
    double get_youngs_modulus() { return youngs_modulus; }
    double get_linear_load() {return linear_load;}
    double get_right_load() {return right_load;}
    double get_left_load() {return left_load;}
};
