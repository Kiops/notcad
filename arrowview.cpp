#include "arrowview.h"
#include <QPainter>

ArrowView::ArrowView(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    m_type_of_row = 0;
}

void ArrowView::paint(QPainter* painter)
{
    //параметры прямоугольника, окружающего стрелку
    qreal half_height = boundingRect().height()/2;
    qreal width = boundingRect().width();
    
    //синусы для расчёта остроты стрелки
    qreal sin_for_typeOfArrow = 0.2249;
    qreal sin_for_singlearrow = 0.1736;
    qreal sin = (m_type_of_row == 2) ? sin_for_typeOfArrow : sin_for_singlearrow;
    qreal arrowhead_length = (width >= 150) ? 50 : 50 * boundingRect().width()/150;
    
    //устанавливаем 
    QPen pen(m_color, 2);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    

    
    //расстановка точек
    QPointF arrowhead_end(width, half_height);
    QPointF arrowtail(0, half_height);
    QPointF arrowhead_start_top(width - arrowhead_length,  sin * 50 + half_height);
    QPointF arrowhead_start_bot(width - arrowhead_length, -sin * 50 + half_height);
    QPointF arrowhead_start_middle(width - arrowhead_length, half_height);
    
    //построение вектора линий по точкам и его отрисовка
    //    painter->drawPolygon(polygon);
    QVector<QLineF> lines;
    
    if(m_type_of_row == 2)
    {
        
        qreal slide = 50;
        lines.push_back(QLineF(arrowtail, arrowhead_end)); //основная прямая
        for(qreal new_width = width - slide; new_width > -slide; new_width -= slide)
        {
            lines.push_back(QLineF(arrowhead_start_top, arrowhead_end));
            lines.push_back(QLineF(arrowhead_start_bot, arrowhead_end));
            
            arrowhead_start_bot.rx() -= slide;
            arrowhead_start_top.rx() -= slide;
            arrowhead_end.rx() -= slide;
            
            //            модуль для отрисовки залитих наконечников
            //            polygon.push_back(arrowhead_end);
            //            polygon.push_back(arrowhead_start_bot);
            //            polygon.push_back(arrowhead_start_top);
            //            path.addPolygon(polygon);
            //            painter->drawPolygon(polygon);
            //                        lines.push_back(QLineF(arrowhead_start_top, arrowhead_end));
            //                        lines.push_back(QLineF(arrowhead_start_bot, arrowhead_end));           
        }
    }
    else
    {   
        //основная прямая
        QLineF main_line(arrowtail, arrowhead_start_middle);
        
        //наконечник стрелки
        QPainterPath path;
        QPolygonF polygon_arrowhead;
        polygon_arrowhead.push_back(arrowhead_end);
        polygon_arrowhead.push_back(arrowhead_start_bot);
        polygon_arrowhead.push_back(arrowhead_start_top);
        path.addPolygon(polygon_arrowhead);
        //в случае двунаправленной стрелки рисуем второй наконечник
        if(m_type_of_row == 1)
        {
            arrowhead_start_top.rx() = arrowhead_length;
            arrowhead_start_bot.rx() = arrowhead_length;
            arrowhead_start_middle.rx() = arrowhead_length;
            main_line.setP1(arrowhead_start_middle);
            QPolygonF polygon_arrowhead_2;
            polygon_arrowhead_2.push_back(arrowtail);
            polygon_arrowhead_2.push_back(arrowhead_start_bot);
            polygon_arrowhead_2.push_back(arrowhead_start_top);
            path.addPolygon(polygon_arrowhead_2);
        }
        painter->fillPath(path, QBrush(QColor(color())));
        lines.push_back(main_line);
    }
    painter->drawLines(lines);
}


QString ArrowView::name() const
{
    return m_name;
}
void ArrowView::setName(const QString& name)
{
    m_name = name;
}

QColor ArrowView::color() const
{
    return m_color;
}
void ArrowView::setColor(const QColor& color)
{
    m_color = color;
}

unsigned int ArrowView::typeOfArrow() const
{
    return m_type_of_row;
}
void ArrowView::setTypeOfArrow(const unsigned int& typeOfArrow)
{
    m_type_of_row = typeOfArrow;
}
