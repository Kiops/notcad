import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Item {
    id: plus
    z: 4
    property color color: "#C5E1A5"
    property var info_for_dialog: undefined
    property var dialog: undefined
    
    signal clicked
    antialiasing: true
    
    width: 50
    height: 50
    
    anchors.centerIn: parent
    
    onClicked: {
        dialog.set_info(info_for_dialog);
        dialog.open();
    }
    
    Rectangle {
        id: circle
        width: parent.width
        height: parent.height
        color: "#333333"
//        color: "transparent"
        radius: 105
        border.width: 20 * Math.sqrt(circle.width*circle.height) / 210
        border.color: plus.color
        
        Rectangle {
            id: rec1
            anchors.centerIn: parent
            width: parent.width*88/210
            height: width/4
            color: plus.color
        }
        
        Rectangle {
            id: rec2
            anchors.centerIn: parent
            width: rec1.height
            height: rec1.width
            color: plus.color
        }
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: parent.clicked()
    }
    
//    DialogBar{
//        id: dialogBar
//    }
}


