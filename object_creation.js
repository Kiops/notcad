.import QtQml 2.0 as QML
.import "double_list.js" as List
.import "interlayer.js" as Interlayer

//заранее загружаем компоненты
var bar_component = Qt.createComponent("Bar.qml");
var plus_component = Qt.createComponent("Plus.qml");
var seal_component = Qt.createComponent("SealView.qml");
var length_marker_component = Qt.createComponent("LengthMarker.qml");

var list = new List.DoubleList();

/*
    ---------------------------------------------------------
    Функция создаёт объект,
    описанный в bar.qml
    между left_neighbor и right_neighbor
    или между left_neighbor/right_neighbor и границей parent
    или между границами parent
    
    left_neighbor - string (qml-id объекта слева)
    right_neighbor - string (qml-id объекта справа)
    ---------------------------------------------------------
*/

function create_bar(list, bar_redactor, bar_params, seals) {
    var left_neighbor = bar_params.left_neighbor;
    var right_neighbor = bar_params.right_neighbor;
    
    var params = {}; //параметры создаваемого объекта
    
    params.id = list.push_between(left_neighbor, right_neighbor); //устанавливаем id нового элемента
    params.seals =seals;
    params.list = list;
    params.bar_redactor = bar_redactor;
    params.my_id_in_list = params.id;
    
    params.ll_value = bar_params.ll_value !== undefined ? bar_params.ll_value : 0;
    params.lpl_value = bar_params.lpl_value !== undefined ? bar_params.lpl_value : 0;
    params.rpl_value = bar_params.rpl_value !== undefined ? bar_params.rpl_value : 0;
    
    params.youngs_modulus = bar_params.youngs_modulus;
    params.max_strain = bar_params.max_strain;
    params.length = bar_params.length;
    params.area = bar_params.area;
    
    if( bar_component.status !== QML.Component.Ready )
    {
        if( bar_component.status === QML.Component.Error )
            console.debug("Error:"+ bar_component.errorString() );
        return;
    }
    
    var sprite = bar_component.createObject(space, params);
    sprite.anchors.verticalCenter = parent.verticalCenter;
    sprite.length_marker = create_length_marker(sprite);
    list.get_node(params.id).data = sprite;
    Interlayer.update_all_x(list, seals);
    
    return params.my_id_in_list;
}

function create_bar_pluses(list, pluses, dialog, bar_centered)
{
    var knots = [];
    if(bar_centered === 1)
        knots = list.get_all(); //в случае центрированных плюсов
    //принимаем за узел нод
    else
        knots = list.get_knots();
    
    knots.forEach(function (knot, i, knots){
        
        //подготовка параметров
        var params = {};
        params.id = "plus_bar_" + i;
        params.dialog = dialog;
        var info_for_dialog = {};
        
        info_for_dialog.list = list;
        info_for_dialog.pluses = pluses;
        
        if(bar_centered === 1)
        {
            info_for_dialog.bar = knot;
            info_for_dialog.bar_centered = bar_centered;
        }
        else
        {
            if(knot.left !== undefined)
                info_for_dialog.left_bar = knot.left.id;
            if(knot.right !== undefined)
                info_for_dialog.right_bar = knot.right.id;
        }
        
        params.info_for_dialog = info_for_dialog;
        params.height = 40;
        params.width = 40;
        
        if(plus_component.status !== QML.Component.Ready )
        {
            if( plus_component.status === QML.Component.Error )
                console.debug("Error:"+ plus_component.errorString() );
            return;
        }
        var sprite = plus_component.createObject(space, params);
        
        //закрепление якоря или координат
        if(knots.length > 1)
        {
            if(bar_centered === 1)
                Interlayer.calculate_coords_for_bar_centered_pluses(knot, sprite);
            else
                Interlayer.calculate_coords_for_circling_bar_pluses(knot, sprite);
        }
        
        pluses.push(sprite);
    })
}

function create_length_marker(bar_sprite)
{
    var params = {};
    var sprite = length_marker_component.createObject(space, params);
    sprite.connected_bar = bar_sprite;
    sprite.x = Qt.binding(function() { return bar_sprite.x});
    sprite.width = Qt.binding(function() { return bar_sprite.width});
    sprite.y = Qt.binding(function() { return bar_sprite.y + bar_sprite.height + 100});
    sprite.text = Qt.binding(function() { return bar_sprite.length});
    return sprite;
}

function create_seal(list, seals, direction)
{
    var params = {};
    params.direction = direction;
    var sprite = seal_component.createObject(space, params);
    if(direction)
    {
        if(seals.left !== undefined)
            Interlayer.remove_seal(seals, seals.left)
        else
        {
//            sprite.x = Qt.binding(function() { return list.get_leftmost().x - sprite.width});
            sprite.x = 0;
            sprite.height = Qt.binding(function() { return list.get_leftmost().height});
            seals.left = sprite;
        }
    }
    else
    {
        if(seals.right !== undefined)
            Interlayer.remove_seal(seals, seals.right);  
        else{
            sprite.x = Qt.binding(function() { return list.get_rightmost().x + list.get_rightmost().width});
            sprite.height = Qt.binding(function() { return list.get_rightmost().height});
            seals.right = sprite;
        }
    }
    Interlayer.update_all_x(list, seals)
    return sprite;
}



function remove_bar(list, pluses, bar, seals)
{
    bar.bar_redactor.bar = undefined;
    list.remove_element(bar.my_id_in_list);
    bar.length_marker.destroy();
    bar.destroy();
    Interlayer.update_all_x(list, seals);
    remove_pluses(pluses);
    //    if(list.length() === 0)
    //    {
    //        if(seals.left !== undefined)
    //            Interlayer.remove_seal(seals, seals.left);
    //        if(seals.right !== undefined)
    //            Interlayer.remove_seal(seals, seals.right);
    //    }
}

function remove_pluses(pluses)
{
    pluses.forEach(function(plus, i, pluses){
        plus.destroy();
    })
    pluses.length = 0;
}

function remove_seal(seals, seal)
{
    //в seal передается либо seals.left либо seals.right
    if(seal !== undefined)
    {
        if(seals.left === seal)
            seals.left = undefined;
        if(seals.right === seal)
            seals.right = undefined;
        seal.destroy();
    }
}


