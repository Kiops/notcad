#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick/QQuickView>

#include "sealview.h"
#include "barview.h"
#include "arrowview.h"
#include "savemodel.h"

#include "construction.h" //mfd
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    
    QLocale::setDefault(QLocale::c());
    qmlRegisterType<ArrowView>("KiopsQML", 1, 0, "ArrowView");
    qmlRegisterType<SaveModel>("KiopsQML", 1, 0, "SaveModel");
    qmlRegisterType<SealView>("KiopsQML", 1, 0, "SealView");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
