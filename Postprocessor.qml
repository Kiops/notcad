import QtQuick 2.0
import QtQuick.Controls 2.2
import KiopsQML 1.0

Item {
    id: postproc
    property var saveModel: undefined
    Column{
        height: childrenRect.height + 10
        width: parent.width
        id: control_column
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        Row{
            spacing: 10
            Label{
                text: "Номер стержня"
                font.pixelSize: 18
            }

            KTextField{
                id: number_of_bar
                only_positive: true
                color: "#EEEEEE"
                textInput.font.pixelSize: 18
                onTextChanged: fill_table()
            }
        }
        Row{
            spacing: 10
            Label{
                text: "Количество отрезков на стержне"
                font.pixelSize: 18
            }
            KTextField{
                id: number_of_segments
                only_positive: true
                color: "#EEEEEE"
                textInput.font.pixelSize: 18
                onTextChanged: fill_table()
            }
        }
    }
    
    KTable{
        id: table
        pixelsize: 18
        anchors.top: control_column.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
    
    function fill_table(){
        var num_of_bar = parseInt(number_of_bar.text);
        if(num_of_bar < 0 || num_of_bar >= saveModel.number_of_bars())
            return;
        var segments_number = parseInt(number_of_segments.text);
        if(segments_number <= 0 || segments_number > 99999)
            return;
        
        var bar_length = saveModel.get_lengths()[num_of_bar];
        var step = bar_length/segments_number;
        
        var path = 0;
        
        table.clear();
        for(var counter = 0; counter <= segments_number; counter++)
        {
            if(counter === segments_number)
                path = bar_length;
            var nx = saveModel.n(path, num_of_bar);
            var ux = saveModel.ux(num_of_bar, path);
            var tension_x = saveModel.tension(path, num_of_bar);
            var max_tension = saveModel.get_maxStrains()[num_of_bar];
            table.append(path, nx, ux, tension_x, max_tension);
            path += step;
        }
    }
}
