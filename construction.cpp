#include "construction.h"

Construction::Construction()
{
    
}

void Construction::clear(){
    bars.clear();
    load_vector.clear();
    shift_matrix.clear();
    solved_equasion.clear();
    right_seal = false;
    left_seal = false;
}

void Construction::fill_construction(Vect areas, Vect lengths, 
                                     Vect maxStrains, Vect youngsModuluses, 
                                     Vect llValues, Vect lplValues, 
                                     Vect rplValues, bool seal_right, 
                                     bool seal_left)
{
    clear();
    for(int i = 0; i < areas.size(); i++)
    {
        push_back(areas[i], lengths[i], maxStrains[i], 
                  youngsModuluses[i],llValues[i], 
                  lplValues[i], rplValues[i]);
    }
    right_seal = seal_right;
    left_seal = seal_left;
}

void Construction::push_back(double area, double length, double maxStrain, 
                             double youngsModuluses, double llValue, 
                             double lplValue, double rplValue)
{
    load_vector.clear();
    shift_matrix.clear();
    bars.push_back(Bar(length, area, maxStrain, youngsModuluses, llValue, rplValue, lplValue));
}

double coefficient(Bar bar){
    return bar.get_youngs_modulus()*bar.get_area()/bar.get_length();
}

Matrix Construction::make_matrix(){
    if(!shift_matrix.empty())
        return shift_matrix;
    Matrix matrix;
    std::vector<double> row;
    int knots = bars.size()+1;
    row.resize(knots, 0);
    matrix.resize(knots, row);
    if(left_seal)
        matrix[0][0] = 1;
    if(right_seal)
        matrix[knots-1][knots-1] = 1;
    const int first_row = left_seal ? 1 : 0;
    const int last_row = right_seal ? knots - 1 : knots;
    for(int i = first_row; i < last_row; i++)
    {
        Bar left_bar;
        Bar right_bar;
        if(i>0)
        {
            left_bar = bars[i-1];
            matrix[i][i-1] = coefficient(left_bar);
            if(((i+2) + (i+2-1))%2 != 0)
                matrix[i][i-1] *= -1;
        }
        //косяк начинается здесь
        if(i<bars.size())
        {
            right_bar = bars[i];
            matrix[i][i+1] = coefficient(right_bar);
            if(((i+2) + (i+3))%2 != 0)
                matrix[i][i+1] *= -1;
        }
        matrix[i][i] = coefficient(left_bar) + coefficient(right_bar);
        if(((i+2) + (i+2))%2 != 0)
            matrix[i][i] *= -1;        
    }
    for(int i = 0; i < knots; i++)
    {
        Bar left_bar;
        Bar right_bar;
        if(i>0)
            left_bar = bars[i-1];
        if(i<bars.size())
            right_bar = bars[i];
        row[i] = left_bar.get_right_load() 
                + left_bar.get_length() * left_bar.get_linear_load()/2 
                + right_bar.get_left_load() 
                + right_bar.get_length() * right_bar.get_linear_load()/2;
    }
    this->shift_matrix = matrix;
    return matrix;
}

std::vector<double> Construction::make_load_vector()
{
    if(!load_vector.empty())
        return load_vector;
    std::vector<double> loads;
    int knots = bars.size()+1;
    loads.resize(knots);
    for(int i = 0; i < knots; i++)
    {
        Bar left_bar;
        Bar right_bar;
        if(i>0)
            left_bar = bars[i-1];
        if(i<bars.size())
            right_bar = bars[i];
        loads[i] = left_bar.get_right_load() 
                + left_bar.get_length() * left_bar.get_linear_load()/2 
                + right_bar.get_left_load() 
                + right_bar.get_length() * right_bar.get_linear_load()/2;
    }
    if(left_seal)
        loads[0] = 0;
    if(right_seal)
        loads[loads.size()-1] = 0;
    load_vector = loads;
    return loads;
}

std::vector<double> Construction::solve_equasion()
{
    if(!solved_equasion.empty())
        return solved_equasion;
    Matrix matrix = make_matrix();
    std::vector<double> result;
    result.resize(matrix.size());
    std::vector<double> loads = make_load_vector();
    for(int i = 0; i < loads.size(); i++)
        matrix[i].push_back(loads[i]);
    
    for(int i = 0; i < matrix.size(); i++)
    {
        int chosen = i;
        for(int j = i; j < matrix[i].size(); j++)
        {
            if(matrix[i][j] != 0)
            {
                chosen = j;
                break;
            }
        }
        if(chosen != i)
        {
            for(int j = i; j < matrix[i].size(); j++)
                std::swap(matrix[j][chosen], matrix[j][i]);
        }
        for(int j = i+1; j < matrix.size(); j++)
        {
            //            if(matrix[j][i] != 0)
            double matrix_j_i = matrix[j][i];
            for(int k = i; k < matrix[j].size(); k++)
            {
                matrix[j][k] += (-matrix_j_i/matrix[i][i] * matrix[i][k]);
            }
        }
    }
    
    for(int i = matrix.size()-1; i >= 0; i--)
    {
        for(int j = matrix.size()-1; j > i; j--)
        {
            matrix[i][matrix.size()] -= matrix[i][j]*result[j];
        }
        result[i] = matrix[i][matrix.size()]/matrix[i][i];
    }
    this->solved_equasion = result;
    return result;
}

double Construction::u(int knot, bool end = 0)
{
    if(end)
        knot++;
    return solve_equasion()[knot];
}

double Construction::ux(int bar_number, double x)
{
    if(bar_number>=bars.size())
        return 0;
    if(bars[bar_number].get_length() < x || x < 0)
        return 0;
    
    double up0 = u(bar_number,0);
    double upl = u(bar_number, 1);
    double ap = bars[bar_number].get_area();
    double lp = bars[bar_number].get_length();
    double qp = bars[bar_number].get_linear_load();
    double ep = bars[bar_number].get_youngs_modulus();
    
    return up0 + x/lp * (upl - up0) + (qp*lp*x)/(2*ep*ap)*(1.0-x/lp);
}

double Construction::N(double x, int p)
{
    double e = bars[p].get_youngs_modulus();
    double q = bars[p].get_linear_load();
    double l = bars[p].get_length();
    double a = bars[p].get_area();
    double up0 = u(p,0);
    double upl = u(p, 1);
    return (e*a/l)*(upl-up0)+(q*l/2)*(1-2*x/l);
}

int Construction::number_of_bars()
{
    return bars.size();
}

double Construction::tension(double x, int p)
{
    return N(x,p)/bars[p].get_area();
}
