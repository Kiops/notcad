function DoubleList() {
    var self = this;

    this.data = {};
    this.size = 0;

    this.id = 0; //переменная для установки id нодам
    this.rightmost = undefined;
    this.leftmost = undefined;

    this.get_node = function (id) {
        return this.data[id];
    };

    this.push_back = function (data_for_node) {

        var node = new Node(data_for_node);
        node.id = self.id;
        if(self.size === 0)
        {
            self.leftmost = self.id;
            self.rightmost = self.id;
        }
        else
        {
            self.data[self.rightmost].right = self.id;
            node.left = self.rightmost;
        }
        self.rightmost = self.id;
        self.data[self.id] = node;
        self.size++;
        self.id++;
        return self.id - 1;
    };

    this.push_front = function (data_for_node) {
        var node = new Node(data_for_node);
        node.id = self.id;
        if(self.size === 0)
        {
            self.leftmost = self.id;
            self.rightmost = self.id;
        }
        else
        {
            self.data[self.leftmost].left = self.id;
            node.right = self.leftmost;
        }
        self.leftmost = self.id;
        self.data[self.id] = node;
        self.size++;
        self.id++;
        return self.id - 1;
    };

    this.push_before = function (data_for_node, id) {
        return self.push_between(undefined, id, data_for_node);
    };

    this.push_after = function (data_for_node, id) {
        return self.push_between(id, undefined, data_for_node);
    };

    this.push_between = function (left, right, data_for_node) {
        if(left === undefined && right === undefined)
        {
            return self.push_back(data_for_node);
        }
        var node = new Node(data_for_node);
        node.id = self.id;
        if (left !== undefined)
        {
            self.get_node(left).right = self.id;
            node.left = left;
            if(left === self.rightmost)
                self.rightmost = self.id;
        }
        if (right !== undefined)
        {
            self.get_node(right).left = self.id;
            node.right = right;
            if(right === self.leftmost)
                self.leftmost = self.id;
        }

        self.data[self.id] = node;
        self.size++;
        self.id++;
        return self.id - 1;
    };

    this.remove_element = function (id) {
        var left = self.get_node(id).left;
        var right = self.get_node(id).right;

        //переназначаем связи соседей
        if (left !== undefined) {
            self.get_node(left).right = right;
        }
        if (right !== undefined) {
            self.get_node(right).left = left;
        }

        //переназначаем крайние ноды, если нужно
        if(id === self.rightmost)
            self.rightmost = left;
        if(id === self.leftmost)
            self.leftmost = right;

        //удаляем нод, уменьшаем размер листа
        delete self.data[id];
        self.size--;
    };

    this.get_all = function () {
        var current_id = self.leftmost;
        var result = [];
        for(var i = 0; i < self.size; i++) {
            result.push(self.get_node(current_id));
            current_id = self.get_node(current_id).right;
        }
        return result;
    };

    this.find_id_by_value = function (value)
    {
        for(var key in self.data)
        {
            if(self.data[key].data === value)
                return key;
        }
    };
    
    this.length = function ()
    {
        return self.size;
    };
    
    this.get_all_before = function (id)
    {
        var result = [];
        var current_id = self.leftmost;
        for(var i = 0; i < self.size && current_id !== id; i++) {
            result.push(self.get_node(current_id));
            current_id = self.get_node(current_id).right;
        }
        return result;
    };

    //возвращает массив узлов, где knot.left - левый нод, knot.right - правый нод
    this.get_knots = function()
    {
        var result = [];
        var first_knot = {};
        if(self.size === 0)
        {
            result.push(first_knot);
            return result;
        }
        var current_id = self.leftmost;
        first_knot.right = self.get_node(self.leftmost);
        result.push(first_knot)
        for(var i = 0; i < self.size; i++) {
            var knot = {}
            knot.left = self.get_node(current_id)
            knot.right= self.get_node(self.get_node(current_id).right)
            result.push(knot)
            current_id = self.get_node(current_id).right;
        }
        return result
    };
    
    this.get_leftmost = function()
    {
        return self.data[self.leftmost].data;
    }
    this.get_rightmost = function()
    {
        return self.data[self.rightmost].data;
    }
}

function Node(data) {
    this.id = undefined;
    this.left = undefined;
    this.right = undefined;
    this.data = data;
}
