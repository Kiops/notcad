import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Item {
    property int pixelsize: 18
    
    function clear(){
        model_postp.clear()
    }
    
    function append(x, nx, ux, tension_x, max_tension_x){
        model_postp.append({"current_x": x, "nx": nx, 
                               "ux": ux, "tension_x": tension_x,
                               "max_tension_x": max_tension_x})
    }
    
    ListModel{
        id: model_postp
    }
    ListView{
        id: listview
        model: model_postp
        anchors.fill: parent
        header:Row{
            height: childrenRect.height
            width: listview.width
            
            Rectangle{
                color: "transparent"
                border.color: "#B0BEC5"
                width: listview.width/5
                height: childrenRect.height
                clip: true
                Label{
                    anchors.centerIn: parent
                    font.pixelSize: pixelsize
                    text: "x"
                }
            }
            Rectangle{
                color: "transparent"
                border.color: "#B0BEC5"
                width: listview.width/5
                height: childrenRect.height
                clip: true
                Label{
                    anchors.centerIn: parent
                    font.pixelSize: pixelsize
                    text: "Nx"
                }
            }
            Rectangle{
                color: "transparent"
                border.color: "#B0BEC5"
                width: listview.width/5
                height: childrenRect.height
                clip: true
                Label{
                    anchors.centerIn: parent
                    font.pixelSize: pixelsize
                    text: "Ux"
                }
            }
            Rectangle{
                color: "transparent"
                border.color: "#B0BEC5"
                width: listview.width/5
                height: childrenRect.height
                clip: true
                Label{
                    anchors.centerIn: parent
                    font.pixelSize: pixelsize
                    text: "σx"
                }
            }
            Rectangle{
                color: "transparent"
                border.color: "#B0BEC5"
                width: listview.width/5
                height: childrenRect.height
                clip: true
                Label{
                    anchors.centerIn: parent
                    font.pixelSize: pixelsize
                    text: "[σ]"
                }
            }
        }
        delegate: Row{
            height: childrenRect.height
            width: listview.width
            Label{
                width: listview.width/5
                font.pixelSize: pixelsize
                text: current_x
                clip: true
            }
            Label{
                width: listview.width/5
                font.pixelSize: pixelsize
                text: nx
                clip: true
            }
            Label{
                width: listview.width/5
                font.pixelSize: pixelsize
                text: ux
                clip: true
            }
            Label{
                width: listview.width/5
                font.pixelSize: pixelsize
                text: tension_x
                clip: true
            }
            Label{
                width: listview.width/5
                font.pixelSize: pixelsize
                text: max_tension_x
                clip: true
            }
        }
    }
}
