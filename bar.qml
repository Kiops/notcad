import QtQuick 2.7
import KiopsQML 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "interlayer.js" as Interlayer

Rectangle
{
    id: main_rect
    
    height: area * 100 * parent.zoom
    width: length * 100 * parent.zoom
    
//    color: "transparent"
    color: "#333333"
    border.color: "#B0BEC5"
    
    anchors.verticalCenter: parent.verticalCenter
    
    property real area: 0
    property real length: 0
    property real max_strain: 0
    property real youngs_modulus: 0
    
    property var my_id_in_list: undefined
    property var list: undefined
    property var seals: undefined
    property var bar_redactor: undefined
    property var length_marker: undefined
    
    property alias ll_value: linear_load.value
    property alias lpl_value: left_point_load.value
    property alias rpl_value: right_point_load.value
    
    onWidthChanged: Interlayer.update_all_x(list, seals)
    
    ArrowView{
        id: left_point_load
        
        typeOfArrow: 0
        color: "#A5D6A7"
        height: (parent.height > 23) ? 23 : parent.height
        width: parent.width/2.5
        
        property real value: 0
        rotation: value < 0 ? 180 : 0
        visible: value !== 0 ? true : false

        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
    }
    ArrowView{
        id: right_point_load
        
        typeOfArrow: 0
        color: "#A5D6A7"
        height: (parent.height > 23) ? 23 : parent.height
        width: parent.width/2.5
        
        property real value: 0
        rotation: value < 0 ? 180 : 0
        visible: value !== 0 ? true : false
        
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
    }
    ArrowView{
        id: linear_load
        
        typeOfArrow: 2
        color: "#A5D6A7"
        width: parent.width
        height: (parent.height > 23) ? 23 : parent.height
        
        property real value: 0
        rotation: value < 0 ? 180 : 0
        visible: value !== 0 ? true : false
        
        anchors.verticalCenter: parent.verticalCenter
    }
    TextInput{
        id: lpl_label;
        color: "#EEEEEE"
        font.pixelSize: 25
        visible: lpl_value !== 0 ? true : false
        anchors.bottom: left_point_load.top
        anchors.horizontalCenter: left_point_load.horizontalCenter
        validator: DoubleValidator {bottom: -Infinity;}
        onEditingFinished: {
            acceptableInput ? lpl_value = text : text = lpl_value
        }

        text: lpl_value > 0 ? lpl_value : -lpl_value;
    }
    TextInput{
        id: rpl_label;
        color: "#EEEEEE"
        font.pixelSize: 25
        visible: rpl_value !== 0 ? true : false
        anchors.bottom: right_point_load.top
        anchors.horizontalCenter: right_point_load.horizontalCenter
        validator: DoubleValidator {bottom: -Infinity;}
        onEditingFinished: {
            acceptableInput ? rpl_value = text : text = rpl_value
        }
        text: rpl_value  > 0 ? rpl_value : -rpl_value;
    }
    TextInput{
        id: ll_label;
        color: "#EEEEEE"
        font.pixelSize: 25
        visible: ll_value !== 0 ? true : false
        anchors.bottom: linear_load.top
        anchors.horizontalCenter: linear_load.horizontalCenter
        validator: DoubleValidator {bottom: -Infinity;}
        onEditingFinished: {
            acceptableInput ? ll_value = text : text = ll_value
        }
        text: ll_value  > 0 ? ll_value : -ll_value;
    }
    
    MouseArea{
        anchors.fill: parent
        onClicked: {
            main_rect.bar_redactor.bar = main_rect;
        }
    }
    
    RectangularGlow {
        id: glow
        z: -1
        visible: false
        anchors.fill: parent
        glowRadius: 10
        spread: 0.2
        color: "green"
        cornerRadius: main_rect.radius + glowRadius
    }
    
    function shine(){
        glow.visible = true;
        main_rect.z = 1;
    }
    function stopShine()
    {
        glow.visible = false;
        main_rect.z = 0;
    }
}


