#include "savemodel.h"
#include <QDataStream>
#include <QVariant>
#include <QFile>
#include <QDebug>

SaveModel::SaveModel(QObject *parent) :
    QObject(parent)
{
    
}

void SaveModel::push_back(qreal area, qreal length, qreal max_strain,
                          qreal youngs_modulus, qreal linear_load,
                          qreal left_point_load, qreal right_point_load)
{
    m_areas.push_back(area);
    m_lengths.push_back(length);
    m_maxStrains.push_back(max_strain);
    m_youngsModuluses.push_back(youngs_modulus);
    m_llValues.push_back(linear_load);
    m_lplValues.push_back(left_point_load);
    m_rplValues.push_back(right_point_load);
}

void SaveModel::clear(){
    m_areas.clear();
    m_lengths.clear();
    m_maxStrains.clear();
    m_youngsModuluses.clear();
    m_llValues.clear();
    m_lplValues.clear();
    m_rplValues.clear();
}

QString SaveModel::write_in_file(QString filename)
{
    if(filename.isEmpty())
        return QString("Error: filename is empty. File was not written.");
    filename += ".kio";
    QFile file(filename);
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QDataStream in(&file);
    in << m_lengths << m_areas << m_maxStrains
       << m_youngsModuluses << m_llValues 
       << m_lplValues << m_rplValues 
       << m_seal_left << m_seal_right;
    file.close();
    return QString("File was written with no errors");
}
void SaveModel::read_from_file(QString filename)
{
    filename += ".kio";
    clear();
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    in >> m_lengths >> m_areas >> m_maxStrains 
            >> m_youngsModuluses >> m_llValues 
            >> m_lplValues >> m_rplValues
            >> m_seal_left >> m_seal_right;
    file.close();
}

QVector <qreal> SaveModel::get_areas()
{
    return m_areas;
}
QVector <qreal> SaveModel::get_lengths()
{
    return m_lengths;
}
QVector <qreal> SaveModel::get_maxStrains()
{
    return m_maxStrains;
}
QVector <qreal> SaveModel::get_youngsModuluses()
{
    return m_youngsModuluses;
}
QVector <qreal> SaveModel::get_llValues()
{
    return m_llValues;
}
QVector <qreal> SaveModel::get_lplValues()
{
    return m_lplValues;
}
QVector <qreal> SaveModel::get_rplValues()
{
    return m_rplValues;
}

void SaveModel::set_seals(bool seal_left, bool seal_right)
{
    m_seal_left = seal_left;
    m_seal_right = seal_right;
}
bool SaveModel::right_seal_exist(){
    return m_seal_right;
}
bool SaveModel::left_seal_exist(){
    return m_seal_left;
}

//препроц
void SaveModel::fill_construction()
{
    construction.fill_construction(m_areas.toStdVector(), m_lengths.toStdVector(), 
                                   m_maxStrains.toStdVector(), m_youngsModuluses.toStdVector(), 
                                   m_llValues.toStdVector(), m_lplValues.toStdVector(), 
                                   m_rplValues.toStdVector(), m_seal_right, 
                                   m_seal_left);
}

//проц
QVector<qreal> SaveModel::make_matrix(int num)
{
    return QVector<double>::fromStdVector(construction.make_matrix()[num]);
}
QVector<qreal> SaveModel::make_load_vector()
{
    return QVector<double>::fromStdVector(construction.make_load_vector());
}
QVector<qreal> SaveModel::solve_equasion()
{
    return QVector<double>::fromStdVector(construction.solve_equasion());
}

//постпроц

double SaveModel::tension(double x, int bar_number)
{
    return construction.tension(x, bar_number);
}

double SaveModel::ux(int bar_number, double x)
{
    return construction.ux(bar_number, x);
}

double SaveModel::n(double x, int bar_number)
{
    return construction.N(x, bar_number);
}

double SaveModel::u(int knot, bool end)
{
    return construction.u(knot, end);
}

int SaveModel::number_of_bars()
{
    return construction.number_of_bars();
}
