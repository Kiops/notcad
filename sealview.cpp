#include "sealview.h"
#include <QPainter>

SealView::SealView(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    m_direction = 0;
}

void SealView::paint(QPainter* painter)
{
    //параметры прямоугольника, окружающего стрелку
    qreal height = boundingRect().height();
    qreal width = boundingRect().width();
    
    //устанавливаем пэн
    QPen pen(m_color, 2);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    
    qreal x1, x2;
    if(direction()){
        x1 = 0;
        x2 = width;
    }
    else{
        x1 = width;
        x2 = 0;
    }
    
    //расстановка точек
    QVector<QLineF> lines;
    QPointF wall_top(x2, height);
    QPointF wall_bot(x2, 0);
    QLineF line(wall_top, wall_bot);
    lines.push_back(line);
    QPointF start;
    QPointF end;
    for(qreal shift = 0; shift < height; shift += 10)
    {
        start.setX(x2);
        start.setY(shift);
        end.setX(x1);
        end.setY(10 + shift);
        line.setPoints(start, end);
        lines.push_back(line);
    }
    
    painter->drawLines(lines);
}


QString SealView::name() const
{
    return m_name;
}
void SealView::setName(const QString& name)
{
    m_name = name;
}

QColor SealView::color() const
{
    return m_color;
}
void SealView::setColor(const QColor& color)
{
    m_color = color;
}

bool SealView::direction() const
{
    return m_direction;
}

void SealView::setDirection(const bool& direction)
{
    m_direction = direction;
}
