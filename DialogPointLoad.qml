import QtQuick 2.7
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import "object_creation.js" as Logic
import "double_list.js" as List

Dialog {
    id: dialog
    
    property var bar: undefined
    property var leftBar: undefined
    property var rightBar: undefined
    property var barCentered: undefined
    
    property var list: undefined    
    property var pluses: undefined
    
    function clear(){
        leftBar = undefined;
        rightBar = undefined;
        txt1.text = "";
    }
    
    function set_info(info_for_dialog)
    {
        leftBar = info_for_dialog.left_bar;
        rightBar = info_for_dialog.right_bar;
        
        bar = info_for_dialog.bar;
        list = info_for_dialog.list;
        pluses = info_for_dialog.pluses;
        barCentered = info_for_dialog.bar_centered;
    }
    
    modality: "ApplicationModal"
    
    contentItem: Rectangle {
        id: main_rect
        anchors.fill: parent
        color: "#2d2d2d"
        implicitWidth: childrenRect.width + 50 //здесь binding loop
        implicitHeight: childrenRect.height + 50 //и здесь (хорошо бы исправить)
        property real text_size: 18
        
        ColumnLayout
        {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 25
            
            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: main_rect.text_size
                id:titile_lab
                text: "Создание точечной нагрузки"
            }
            
            RowLayout{
                spacing: 7
                Label{
                    id:lab1
                    text: "Значение"
                    font.pixelSize: main_rect.text_size
                }
                KTextField{
                    id: txt1
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                }
            }
            RowLayout{
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    text: "Отмена"
                    onClicked: {
                        clear();
                        close();
                    }
                }
                Button{
                    text: "Создать"
                    onClicked: 
                    {   
                        if(dialog.barCentered === 1) 
                            dialog.bar.data.ll_value = txt1.text;
                        else if(leftBar !== undefined)
                            dialog.list.get_node(leftBar).data.rpl_value = txt1.text;
                        else if(rightBar !== undefined)
                            dialog.list.get_node(rightBar).data.lpl_value = txt1.text;
                        clear();
                        close();
                    }
                }
            }
        }
    }
}


