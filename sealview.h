#pragma once

#include <QtQuick/QQuickPaintedItem>
#include <QColor>

class SealView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QColor color READ color  WRITE setColor)
    Q_PROPERTY(bool direction READ direction WRITE setDirection)
    
    bool m_direction;
    QString m_name;
    QColor m_color;
    
public:
    SealView(QQuickItem *parent = 0);
    
    QString name() const;
    void setName(const QString& name);
    
    QColor color() const;
    void setColor(const QColor& color);
    
    bool direction() const;
    void setDirection(const bool& direction);
    
    void paint(QPainter *painter);    
   };
