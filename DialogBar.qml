import QtQuick 2.7
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import "object_creation.js" as Logic

Dialog {
    id: dialog
    
    property var leftBar: undefined
    property var rightBar: undefined
    
    property var list: undefined
    property var seals: undefined
    property var pluses: undefined
    property var bar_redactor: undefined
    
    function clear(){
        leftBar = undefined;
        rightBar = undefined;
        txt1.text = "";
        txt2.text = "";
        txt3.text = "";
        txt4.text = "";
    }
    
    function set_info(info_for_dialog)
    {
        leftBar = info_for_dialog.left_bar
        rightBar = info_for_dialog.right_bar
        
        list = info_for_dialog.list
        pluses = info_for_dialog.pluses
    }
    
    modality: "ApplicationModal"
    
    contentItem: Rectangle {
        id: main_rect
        anchors.fill: parent
        color: "#2d2d2d"
        implicitWidth: childrenRect.width + 50 //здесь binding loop
        implicitHeight: childrenRect.height + 50 //и здесь (хорошо бы исправить)
        property real text_size: 18
        
        
        
        ColumnLayout
        {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 25
            
            Label{
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: main_rect.text_size
                id:titile_lab
                text: "Создание стержня"
            }
            
            RowLayout{
                spacing: 7
                Label{
                    id:lab1
                    text: "Длина"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: main_rect.text_size
                }
                KTextField{
                    id: txt1
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    
                    KeyNavigation.up: create
                    KeyNavigation.down: txt2
                }
            }
            RowLayout{
                spacing: 7
                anchors.left: parent.left
                anchors.right: parent.right
                Label{
                    id:lab2
                    text: "Площадь поперечного сечения"
                    Layout.minimumWidth: childrenRect.width
                    font.pixelSize: main_rect.text_size
                }
                KTextField{
                    id: txt2
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    
                    KeyNavigation.up: txt1
                    KeyNavigation.down: txt3
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab3
                    text: "Модуль Юнга"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: main_rect.text_size
                }
                KTextField{
                    id: txt3
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    
                    KeyNavigation.up: txt2
                    KeyNavigation.down: txt4
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab4
                    text: "Допустимое напряжение"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: main_rect.text_size
                }
                KTextField{
                    id: txt4
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    
                    KeyNavigation.up: txt3
                    KeyNavigation.down: create
                }
            }
            RowLayout{
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: cancel
                    text: "Отмена"
                    onClicked: {
                        clear();
                        close();
                    }
                    
                    KeyNavigation.left: create
                    KeyNavigation.right: create
                    KeyNavigation.up: txt4
                    KeyNavigation.down: txt1
                    
                    //                    Keys.onSpacePressed: console.log("Yeah")
                }
                Button{
                    id: create
                    text: "Создать"
                    onClicked: 
                    {
                        if(txt1.acceptableInput && txt2.acceptableInput &&
                                txt3.acceptableInput && txt4.acceptableInput)
                        {
                            var bar_params = {};
                            
                            bar_params.left_neighbor = leftBar;
                            bar_params.right_neighbor = rightBar;
                            
                            bar_params.youngs_modulus = txt3.text;
                            bar_params.max_strain = txt4.text;
                            bar_params.length = txt1.text;
                            bar_params.area = txt2.text;
                            
                            Logic.create_bar(list, bar_redactor, bar_params, seals);
                            Logic.remove_pluses(pluses)
                            Logic.create_bar_pluses(list, pluses, dialog);
                            clear();
                            close();
                        }
                    }
                    
                    KeyNavigation.left: cancel
                    KeyNavigation.right: cancel
                    KeyNavigation.up: txt4
                    KeyNavigation.down: txt1
                    
                    Keys.onEnterPressed: click()
                }
            }
        }
    }
}


