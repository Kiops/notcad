#pragma once

#include <QtQuick/QQuickPaintedItem>
#include <QColor>

class BarView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QColor color READ color  WRITE setColor )
    
    //параметры стержня
    Q_PROPERTY(double area READ area WRITE setArea NOTIFY areaChanged)
    Q_PROPERTY(double length READ length WRITE setLength NOTIFY lengthChanged)
    Q_PROPERTY(double maxStrain READ maxStrain WRITE setMaxStrain NOTIFY maxStrainChanged)
    Q_PROPERTY(double youngsModulus READ youngsModulus WRITE setYoungsModulus NOTIFY youngsModulusChanged)
    
    //параметры нагрузок
    Q_PROPERTY(double leftForce READ leftForce WRITE setLeftForce NOTIFY leftForceChanged)
    Q_PROPERTY(double rightForce READ rightForce WRITE setRightForce NOTIFY rightForceChanged)
    Q_PROPERTY(double linearForce READ linearForce WRITE setLinearForce NOTIFY linearForceChanged)
    
    
    QString m_name;
    QColor m_color;
    
    //параметры стержя
    double m_area;
    double m_length;
    double m_maxStrain;
    double m_youngsModulus;
    
    //параметры нагрузок
    double m_leftForce;
    double m_rightForce;
    double m_linearForce;
    
public:
    BarView(QQuickItem *parent = 0);
    
    QString name() const;
    void setName(const QString& name);
    
    QColor color() const;
    void setColor(const QColor& color);
   
    //параметры стержня
    
    double area() const;
    void setArea(const double& area) const;
    
    double length() const;
    void setLength(const double& length) const;
    
    double maxStrain() const;
    void setMaxStrain(const double& maxStrain);
    
    double youngsModulus() const;
    void setYoungsModulus(const double& youngsModulus);
    
    //нагрузки
    
    double leftForce() const;
    void setLeftForce  (const double& leftForce);
    
    double rightForce() const;
    void setRightForce (const double& rightForce);

    double linearForce() const;
    void setLinearForce  (const double& linearForce);
    
    void paint(QPainter *painter);
    
signals:
    void areaChanged(double area);
    void lengthChanged(double length);
    void maxStrainChanged(double maxStrain);
    void youngsModulusChanged(double youngsModulus);
    void leftForceChanged(double leftForce);
    void rightForceChanged(double rightForce);
    void linearForceChanged(double linearForce);
};
