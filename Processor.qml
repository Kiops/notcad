import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    property var saveModel: undefined
    function reboot_text(){
        //заполняем модель
        saveModel.fill_construction();
        
        var str = "[ "
        var matrix_size = saveModel.make_matrix(0).length;
        for(var i = 0; i < matrix_size; i++)
        {
            var row = saveModel.make_matrix(i);
            for(var j = 0; j < row.length; j++)
                str += (Math.round(row[j]*100)/100) + " "
            if (i !== matrix_size - 1)str += "\n ";
        }
        str += "]"
        matrix.text = str
        
        var loads_vec = saveModel.make_load_vector();
        str = "[ "
        for(var i = 0; i < loads_vec.length; i++)
        {
            str += (Math.round(loads_vec[i]*100)/100) + " ";
        }
        str += "]"
        load_vector.text = str
        
        str = ""
        for(var i = 0; i < matrix_size; i++)
        {
            var row = saveModel.make_matrix(i);
            var str_is_empty = 1;
            for(var j = 0; j < row.length; j++)
            {
                if(row[j] !== 0){
                    if(row[j] > 0 && !str_is_empty)
                        str += "+"
                    str_is_empty = 0;
                    str += (Math.round(row[j]*100)/100) + "EA/L * Δ" + (j+1) + " "
                }
            }
            str += "= " + loads_vec[i] + "F";
            if (i !== matrix_size - 1)str += "\n";
        }
        equasion_system.text = str;
        
        str = ""
        var solved = saveModel.solve_equasion();
        for(var i = 0; i < solved.length; i++)
        {
            str += "Δ" + (i+1) + " = " + (Math.round(solved[i]*100)/100) + " * FL/EA"
            if (i !== solved.length - 1)str += "\n";
        }
        solved_equasion.text = str
        
    }
    
    GroupBox{
        id: g_matrix
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        title: "[A]"
        Text {
            id: matrix
            anchors.centerIn: parent
            font.pixelSize: 15;
            color: "white"
            
        }
    }
    GroupBox{
        id: g_load_vector
        title: "[B]"
        anchors.top: g_matrix.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Text{
            id: load_vector
            anchors.centerIn: parent
            font.pixelSize: 15;
            color: "white"    
        }
    }
    GroupBox{
        title: "Система уравнений"
        id: g_equasion_system
        anchors.top: g_load_vector.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        
        Text{
            id: equasion_system
            anchors.centerIn: parent
            font.pixelSize: 15;
            color: "white"    
        }
    }
    GroupBox{
        title: "Полученный глобальный вектор перемещений"
        id: g_solved_equasion
        anchors.top: g_equasion_system.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Text{
            anchors.centerIn: parent
            id: solved_equasion
            font.pixelSize: 15;
            color: "white"    
        }
    }
}
