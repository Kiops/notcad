.import "double_list.js" as List
.import QtQml 2.7 as QML

//рассчиывает координату x для каждого элемента стержня
function update_all_x(list, seals) {
    var nodes = list.get_all();
    var summ_before = 0;
//    if(seals.left !== undefined)
//    {
//        summ_before += seals.left.x;
//        summ_before += seals.left.width;
//    }
    
    var last_bar = undefined;
    
    nodes.forEach(function(node, number, nodes){
        if(last_bar === undefined)
        {
            if(seals.left !== undefined)
                node.data.x = Qt.binding(function(){return seals.left.x + seals.left.width});
            else
                node.data.x = 0;
        }
        else
        {
//            node.data.x = Qt.binding(function(){return last_bar.data.x + last_bar.data.width});
            node.data.x = last_bar.data.x + last_bar.data.width;
        }
        last_bar = node;
    });
    if(seals.left !== undefined)
    {
        seals.left.x = Qt.binding(function() { return list.get_leftmost().x - seals.left.width});
        seals.left.height = Qt.binding(function() { return list.get_leftmost().height});
    }
    if(seals.right !== undefined)
    {
        seals.right.x = Qt.binding(function() { return list.get_rightmost().x + list.get_rightmost().width});
        seals.right.height = Qt.binding(function() { return list.get_rightmost().height});
    }
    if(list.length() === 0)
    {
        if(seals.left !== undefined)
            remove_seal(seals, seals.left);
        if(seals.right !== undefined)
            remove_seal(seals, seals.right);
    }
}

function calculate_coords_for_circling_bar_pluses(knot, sprite)
{
    sprite.anchors.centerIn = undefined;
    if(knot.left !== undefined)
    {
        sprite.x = Qt.binding(function() { return knot.left.data.x + knot.left.data.width - sprite.width/2;})
        sprite.y = Qt.binding(function() { return knot.left.data.y + knot.left.data.height/2 - sprite.height/2;})
    }
    else if(knot.right !== undefined)
    {
        sprite.x = Qt.binding(function() { return knot.right.data.x - sprite.width/2;})
        sprite.y = Qt.binding(function() { return knot.right.data.y + knot.right.data.height/2 - sprite.height/2;})
    }
}

function calculate_coords_for_bar_centered_pluses(knot, sprite)
{
    sprite.anchors.centerIn = undefined;
    sprite.x = Qt.binding(function() { return knot.data.x + knot.data.width/2 - sprite.width/2;})
    sprite.y = Qt.binding(function() { return knot.data.y + knot.data.height/2 - sprite.height/2;})
}

function remove_seal(seals, seal)
{
    //в seal передается либо seals.left либо seals.right
    if(seals !== undefined)
    {
        if(seals.left === seal)
            seals.left = undefined;
        if(seals.right === seal)
            seals.right = undefined;
        seal.destroy();
    }
}
