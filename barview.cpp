#include "barview.h"
#include <QPainter>

BarView::BarView(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    
}

void BarView::paint(QPainter* painter)
{
    int m_length = 2;
    int m_area = 1;
//    QPen pen(m_color, 2);
//    painter->setPen(pen);
////    QRectF rectrangle(QPointF(0,0),QSizeF(m_length, m_area));
//    painter->setRenderHints(QPainter::Antialiasing, true);
//    painter->drawRect(0, 0, m_length, m_area);
    QPen pen(m_color, 2);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawPie(boundingRect().adjusted(1,1,-1,-1),90*16, 290*16);
//    painter->drawRect(0, 0, 100*m_length, 100*m_area);
    painter->drawRect(boundingRect().adjusted(1,1,-1,-1));
}


QString BarView::name() const
{
    
}
void BarView::setName(const QString& name)
{
    
}

QColor BarView::color() const
{
    
}
void BarView::setColor(const QColor& color)
{
    m_color = color;
}


double BarView::area() const
{
    
}
void BarView::setArea(const double& area) const
{
    
}

double BarView::length() const
{
    
}
void BarView::setLength(const double& length) const
{
    
}

double BarView::maxStrain() const
{
    
}
void BarView::setMaxStrain(const double& maxStrain)
{
    
}

double BarView::youngsModulus() const
{
    
}
void BarView::setYoungsModulus(const double& youngsModulus)
{
    
}

double BarView::linearForce() const
{
    
}
void BarView::setLinearForce  (const double& linearForce)
{
    
}

double BarView::leftForce() const
{
    
}
void BarView::setLeftForce  (const double& leftForce)
{
    
}
double BarView::rightForce() const
{
    
}
void BarView::setRightForce (const double& rightForce)
{
    
}
