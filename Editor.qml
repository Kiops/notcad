import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import KiopsQML 1.0

import "object_creation.js" as Logic
import "double_list.js" as List
import "SaveAndLoad.js" as SaveAndLoad


Item {
    
    id:editor
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    
    property var seals: {return {}}
    property var pluses: {return []}
    property var saveModel: undefined
    property var bar_list: {return new List.DoubleList()}
    
    
    Flickable{
        id:flick
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: sidebar.left
        anchors.bottom: parent.bottom
        Item{
            id: space
            x: width > parent.width ? -hbar.position * width : 0
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: width <= parent.width ? parent.horizontalCenter : undefined
            height: parent.height
            width: childrenRect.width
            property real zoom: 1
        }
        ScrollBar {
            id: hbar
            hoverEnabled: true
            active: hovered || pressed
            orientation: Qt.Horizontal
            size: flick.width / space.width
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
        MouseArea{
            id: m_area
            z: -1
            anchors.fill: parent
            onClicked: {
                barRedactor.bar = undefined;
                forceActiveFocus();
            }
            onWheel: {
                if(wheel.angleDelta.y/1200 + space.zoom > 0)
                    space.zoom += wheel.angleDelta.y/1200;
            }
        }
    }
    
    Rectangle{
        id:sidebar
        width: childrenRect.width
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        color: "#484848"
        GroupBox{
            id: filepanel
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            title: "Работа с файлами"
            ColumnLayout
            {
                RowLayout{
                    spacing: 7
                    Label{
                        id: filename_label
                        text: "Имя файла"
                        font.pixelSize: barRedactor.text_size
                    }
                    KTextField{
                        id: filename
                        color: "#EEEEEE"
                        textInput.font.pixelSize: barRedactor.text_size
                        textInput.validator: undefined
                    }
                }
                RowLayout{
                    Button{
                        text: "Загрузить"
                        onClicked:{
                            SaveAndLoad.load(bar_list, seals, pluses, barRedactor, saveModel, filename.text);
                            switches_groupbox.uncheck_switches();
                            Logic.remove_pluses(pluses);
                        }
                    }
                    Button{
                        text: "Сохранить"
                        onClicked: {
                            SaveAndLoad.save(bar_list, seals, saveModel);
                            saveModel.write_in_file(filename.text);
                        }
                    }
                }
                RowLayout{
                    
                    Button{
                        text: "Левая заделка"
                        onClicked: {
                            if(bar_list.length() !== 0)
                            {
                                Logic.create_seal(bar_list, seals, true);
                            }
                        }
                    }
                    Button{
                        text: "Правая заделка"
                        onClicked: {
                            if(bar_list.length() !== 0)
                            {
                                Logic.create_seal(bar_list, seals, false);
                            }
                        }
                    }
                }
            }
        }
        GroupBox{
            id: switches_groupbox
            title: "Установка конструкции"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: filepanel.bottom
            function uncheck_switches()
            {
                switch1.checked = 0;
                switch2.checked = 0;
                switch3.checked = 0;
            }
            
            Column {
                id: column
                Switch {
                    id: switch1
                    text: "Стержни"
                    onClicked: {
                        if(checked == true){
                            switch2.checked = false;
                            switch3.checked = false;
                            
                            Logic.remove_pluses(pluses);
                            Logic.create_bar_pluses(bar_list, pluses, dialogBar);
                        }
                        else
                            Logic.remove_pluses(pluses)
                    }
                }
                Switch {
                    id: switch2
                    text: "Точечные нагрузки"
                    onClicked: {
                        if(bar_list.length() > 0){
                            if(checked == true){
                                switch1.checked = false;
                                switch3.checked = false;
                                
                                Logic.remove_pluses(pluses);
                                Logic.create_bar_pluses(bar_list, pluses, dialogPointLoad);
                            }
                            else
                                Logic.remove_pluses(pluses);
                        }
                        else
                            checked = false;
                    }
                }
                Switch {
                    id: switch3
                    text: "Распределенные нагрузки"
                    onClicked: {
                        if(bar_list.length() > 0){
                            if(checked == true){
                                switch1.checked = false;
                                switch2.checked = false;
                                
                                Logic.remove_pluses(pluses);
                                Logic.create_bar_pluses(bar_list, pluses, dialogPointLoad, 1);
                            }
                            else
                                Logic.remove_pluses(pluses)
                        }
                        else checked = false;
                    }
                }
            }
        }
        BarRedactor{
            id: barRedactor
            seals: editor.seals
            list: bar_list
            pluses: editor.pluses
            anchors.top: switches_groupbox.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: childrenRect.width
            onUncheckSwitches: switches_groupbox.uncheck_switches()
        }
    }
    
    DialogBar{
        id: dialogBar
        bar_redactor: barRedactor
        seals: editor.seals
    }
    DialogPointLoad{
        id: dialogPointLoad
    }
    
}
