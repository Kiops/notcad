import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import "object_creation.js" as Logic

Item {
    id: redactor
    
    property var bar: undefined
    property var fuzzyBar: undefined
    property var list: undefined
    property var pluses: undefined
    property var seals: undefined
    signal uncheckSwitches()
    
    property real text_size: 18
    GroupBox{
        title: "Редактирование стержня"
        ColumnLayout
        {
            anchors.left: parent.left
            RowLayout{
                spacing: 7
                Label{
                    id:lab1
                    text: "Длина"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt1
                    text: bar !== undefined ? bar.length : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    onInputFinished: {
                        textInput.acceptableInput ? bar.length = text : text = bar.length
                    }
                    KeyNavigation.up: txt7
                    KeyNavigation.down: txt2
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                anchors.left: parent.left
                anchors.right: parent.right
                Label{
                    id:lab2
                    text: "Площадь поперечного сечения"
                    Layout.minimumWidth: childrenRect.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt2
                    text: bar !== undefined ? bar.area : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    onInputFinished: {
                        textInput.acceptableInput ? bar.area = text : text = bar.area
                    }
                    
                    KeyNavigation.up: txt1
                    KeyNavigation.down: txt3
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab3
                    text: "Модуль Юнга"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt3
                    text: bar !== undefined ? bar.youngs_modulus : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    onInputFinished: {
                        textInput.acceptableInput ? bar.youngs_modulus = text : text = bar.youngs_modulus
                    }
                    
                    KeyNavigation.up: txt2
                    KeyNavigation.down: txt4
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab4
                    text: "Допустимое напряжение"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt4
                    text: bar !== undefined ? bar.max_strain : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: true
                    onInputFinished: {
                        textInput.acceptableInput ? bar.max_strain = text : text = bar.max_strain
                    }
                    
                    KeyNavigation.up: txt3
                    KeyNavigation.down: txt5
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab5
                    text: "Точечная нагрузка слева"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt5
                    text: bar !== undefined ? bar.lpl_value : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: false
                    onInputFinished: {
                        textInput.acceptableInput ? bar.lpl_value = text : text = bar.lpl_value
                    }
                    
                    KeyNavigation.up: txt4
                    KeyNavigation.down: txt6
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab6
                    text: "Точечная нагрузка справа"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt6
                    text: bar !== undefined ? bar.rpl_value : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: false
                    onInputFinished: {
                        textInput.acceptableInput ? bar.rpl_value = text : text = bar.rpl_value
                    }
                    
                    KeyNavigation.up: txt5
                    KeyNavigation.down: txt7
                    disabled: bar !== undefined ? false : true
                }
            }
            RowLayout{
                spacing: 7
                Label{
                    id:lab7
                    text: "Погонная нагрузка"
                    Layout.minimumWidth: lab2.width
                    font.pixelSize: redactor.text_size
                }
                KTextField{
                    id: txt7
                    text: bar !== undefined ? bar.ll_value : ""
                    color: lab1.color
                    textInput.font.pixelSize: lab1.font.pixelSize
                    only_positive: false
                    onInputFinished: {
                        textInput.acceptableInput ? bar.ll_value = text : text = bar.ll_value
                    }
                    
                    KeyNavigation.up: txt6
                    KeyNavigation.down: txt1
                    disabled: bar !== undefined ? false : true
                }
            }
            Button{
                text: "Удалить стержень"
                onClicked: {
                    Logic.remove_bar(redactor.list, redactor.pluses, bar, redactor.seals);
                    redactor.uncheckSwitches();
                }
            }
        }
    }
    onBarChanged: {
        if(fuzzyBar !== undefined)
            fuzzyBar.stopShine();
        fuzzyBar = bar;
        if(bar !== undefined)
            bar.shine();
    }
}

