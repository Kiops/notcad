#include <QObject>
#include <QVector>
#include <QString>
#include <QDebug>
#include <QJSValue>

#include "construction.h"

using Vec = QVector<qreal>;

class SaveModel : public QObject
{
    Q_OBJECT
    

public:
    explicit SaveModel(QObject *parent = nullptr);
    
    Q_INVOKABLE void push_back(qreal area, qreal length, qreal max_strain,
                               qreal youngs_modulus, qreal linear_load,
                               qreal left_point_load, qreal right_point_load);
    
    Q_INVOKABLE QString write_in_file(QString filename);
    Q_INVOKABLE void read_from_file(QString filename);
    Q_INVOKABLE void clear();
    
    Q_INVOKABLE void set_seals(bool seal_left, bool seal_right);
    Q_INVOKABLE bool right_seal_exist();
    Q_INVOKABLE bool left_seal_exist();
    
    Q_INVOKABLE QVector <qreal> get_areas();
    Q_INVOKABLE QVector <qreal> get_lengths();
    Q_INVOKABLE QVector <qreal> get_maxStrains();
    Q_INVOKABLE QVector <qreal> get_youngsModuluses();
    Q_INVOKABLE QVector <qreal> get_llValues();
    Q_INVOKABLE QVector <qreal> get_lplValues();
    Q_INVOKABLE QVector <qreal> get_rplValues();
    
    Q_INVOKABLE void fill_construction();
    Q_INVOKABLE QVector<qreal> make_matrix(int num);
    Q_INVOKABLE QVector<qreal> make_load_vector();
    Q_INVOKABLE QVector<qreal> solve_equasion();
    
    Q_INVOKABLE double tension(double x, int bar_number);
    Q_INVOKABLE double ux(int bar_number, double x);
    Q_INVOKABLE double n(double x, int bar_number);
    Q_INVOKABLE double u(int knot, bool end);
    Q_INVOKABLE int number_of_bars();
    
private:
    
    Construction construction;
    
    Vec m_areas;
    Vec m_lengths;
    Vec m_maxStrains;
    Vec m_youngsModuluses;
    Vec m_llValues;
    Vec m_lplValues;
    Vec m_rplValues;
    bool m_seal_right;
    bool m_seal_left;
};
