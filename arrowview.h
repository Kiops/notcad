#pragma once

#include <QtQuick/QQuickPaintedItem>
#include <QColor>

class ArrowView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QColor color READ color  WRITE setColor)
    Q_PROPERTY(unsigned int typeOfArrow READ typeOfArrow WRITE setTypeOfArrow)
    
    unsigned int m_type_of_row; // 0 - стандартная стерлка, 1 - двунаправленная, 2 - погонная
    QString m_name;
    QColor m_color;
    
public:
    ArrowView(QQuickItem *parent = 0);
    
    QString name() const;
    void setName(const QString& name);
    
    QColor color() const;
    void setColor(const QColor& color);
    
    unsigned int typeOfArrow() const;
    void setTypeOfArrow(const unsigned int& typeOfArrow);
    
    void paint(QPainter *painter);    
   };
