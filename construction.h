#pragma once
#include <vector>
#include "bar.h"

using Vect = std::vector<double>;
using Matrix = std::vector<std::vector<double>>;

class Construction
{
    std::vector <Bar> bars;
    
    Matrix shift_matrix; //матрица перемещений
    std::vector<double> load_vector; //вектор нагрузок относительно узлов
    std::vector<double> solved_equasion; //вектор решения уравнения
public:
    bool left_seal;
    bool right_seal;
    Construction();
    void clear();
    void push_back(double area, double length,
                   double maxStrain, double youngsModuluses,
                   double llValue, double lplValue, double rplValue);
    void fill_construction(Vect areas,
                           Vect lengths,
                           Vect maxStrains,
                           Vect youngsModuluses,
                           Vect llValues,
                           Vect lplValues,
                           Vect rplValues,
                           bool seal_right,
                           bool seal_left);
    Matrix make_matrix();
    std::vector<double> make_load_vector();
    std::vector<double> solve_equasion();
    double u(int knot, bool end);
    double ux(int bar_number, double x);
    double N(double x, int p);
    double tension(double x, int p);
    int number_of_bars();
};


