import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import KiopsQML 1.0
import "SaveAndLoad.js" as SaveAndLoad

ApplicationWindow {
    id: mainwindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Notcad")
    visibility: "Maximized"
    
    SaveModel{
        id:saveModel
    }
    
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        onCurrentIndexChanged:{
            if(editor.bar_list !== 0 && (editor.seals.left !== undefined || editor.seals.right !== undefined))
            {
                SaveAndLoad.save(editor.bar_list, editor.seals, saveModel);
                proc.reboot_text();
            }
            else
            {
                setCurrentIndex(0);
            }
        }
        
        Page{
            Editor{
                id: editor
                saveModel: saveModel
                anchors.fill: parent;
            }
        }
        
        
        Page {
            Processor{
                id: proc
                saveModel: saveModel
                anchors.fill: parent
            }
        }
        
        Page{
            Postprocessor{
                id: postproc
                saveModel: saveModel
                anchors.fill: parent
            }
        }
    }
    
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Препроцессор")
        }
        TabButton {
            text: qsTr("Процессор")
        }
        TabButton {
            text: qsTr("Постпроцессор")
        }
    }
}
